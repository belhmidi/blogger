﻿using Blog.Controllers;
using Blog.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Threading.Tasks;

namespace BlogUnitTest
{
    [TestClass]
    public class CategoriesTest
    {
        [TestMethod]
        public void Index_Test()
        {
            //Arrange
            var context = new FakeDbContext { Categories = { new Categories { Libelle = "Biologie" } } };
            CategoriesController categ = new CategoriesController(context);
            
            //Act
            ViewResult result = categ.Index() as ViewResult;
            var categorie = (IEnumerable<Categories>)result.ViewData.Model;

            //Assert
            Assert.IsInstanceOfType(result.ViewData.Model, typeof(IEnumerable<Categories>));
            Assert.AreEqual("Biologie", categorie.ElementAt(0).Libelle);
        }

        [TestMethod]
        public void Method_test()
        {           
            //Arrange
            CategoriesController ctr = new CategoriesController();
            // Act
            ViewResult createCat = ctr.Create() as ViewResult;            
            //Assert
            Assert.IsNotNull(createCat);
            Assert.IsNotNull(string.Empty, createCat.ViewName);
            Assert.IsInstanceOfType(createCat, typeof(ViewResult));
            Assert.AreEqual(string.Empty, createCat.ViewName);
        }

        [TestMethod]
        public void Cree_Categorie_test()
        {
            // Arrange
            var context = new FakeDbContext { };
            Categories objCateg = new Categories();
            //Act  
            objCateg.idCategorie = 11;
            objCateg.Libelle = "Test libellee";
            CategoriesController categController = new CategoriesController(context);
            RedirectToRouteResult result = categController.Creer(objCateg) as RedirectToRouteResult;
            //Assert           
            Assert.AreEqual("Index", result.RouteValues["Action"]);
            Assert.IsNotNull(result.ToString());
        }


    [TestMethod]
    public void Details_Id_isNotNull_Test()
    { 
        var context = new FakeDbContext { Categories = { new Categories { idCategorie= 1, Libelle = "Recette" } } };
        CategoriesController Categories = new CategoriesController(context);
        ViewResult result = Categories.Details(1) as ViewResult;

        Assert.IsInstanceOfType(result.ViewData.Model, typeof(Categories));
        var categ = result.ViewData.Model as Categories;
        Assert.AreEqual(1, categ.idCategorie);
        Assert.AreEqual("Recette", categ.Libelle);
    }

        [TestMethod]
        public void Details_Id_isNull_Test()
        {
            //Arrange
            var context = new FakeDbContext { };
            CategoriesController Categories = new CategoriesController(context);

            // Act
            var result = Categories.Details(null) as RedirectToRouteResult;
            
            //Assert
            Assert.IsNotNull(result);   
            Assert.AreEqual("Index", result.RouteValues["action"]);
            Assert.AreEqual("Categories", result.RouteValues["controller"]);
        }
    }
}