﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Blog.Models
{
    [MetadataType(typeof(UtilisateursMetaData))]
    public partial class Utilisateurs
    {

    }
    public class UtilisateursMetaData
    {
        [Display(Name = "Auteur ")]
        public int nom { get; set; }
    }
}