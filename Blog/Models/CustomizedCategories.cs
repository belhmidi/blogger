﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Blog.Models
{
    [MetadataType(typeof(CategoriesMetaData))]
    public partial class Categories
    {

    }
    public class CategoriesMetaData
    {
        [Display(Name = "Catégorie ")]
        public string Libelle { get; set; }
    }


}