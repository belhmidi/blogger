﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Blog.Models
{
    [MetadataType(typeof(CommentaireMetaData))]
    public partial class Commentaires
    {

    }
    public class CommentaireMetaData
    {
        [Display(Name = "Publié le")]  
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime datePublication { get; set; }

        //[Required(ErrorMessage = "Le contenu est obligatoire")]
        [Display(Name = "Contenu ")]
        public string contenu { get; set; }
        
    }
}