﻿using Blog.Models;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Blog.Controllers
{
    public class CategoriesController : Controller
    {
        //BlogsContext db = new BlogsContext();

        private readonly IContext db;

        public CategoriesController()
        {
            db = new BlogsContext();
        }
        public CategoriesController(IContext context)
        {
            db = context;
        }

        // GET: Categories
        public ActionResult Index()
        {
            return View(db.Categories.ToList());            
        }


        // GET: Categories/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                return RedirectToAction("Index", "Categories");

            }
            
           Categories categ = db.Categories.Find(id);
            if (categ == null)
            {
                return HttpNotFound();
            }
            return View(categ);
        }
        // GET: Categories/Create
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Creer(Categories categ)
        {
            db.Categories.Add(categ);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // POST: Categories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Categories categories)
        {         

            if (ModelState.IsValid)
            {
                db.Categories.Add(categories);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(categories);
        }
        // GET: Categories/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Categories categories = db.Categories.Find(id);
            if (categories == null)
            {
                return HttpNotFound();
            }
            return View(categories);
        }

        // POST: Categories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idCategorie,Libelle")] Categories categories)
        {
            if (ModelState.IsValid)
            {               
                //db.Entry(categories).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(categories);
        }

        // GET: Categories/Total articles
        public int GetTotalArticle(int id)
        {            
            int TotalArticle = db.Articles.Where(x => x.idCategorie == id).Count();

            return TotalArticle;
        }

        // GET: Categories/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Categories categories = db.Categories.Find(id);
            if (categories == null)
            {
                return HttpNotFound();
            }
            if ((GetTotalArticle((int)id) != 0))
            {
                return new ContentResult
                {
                    Content = "Impossible d'effectuer la suppression, car cette catégorie contient des articles !!!!"
                };
            }
            return View(categories);
        }

        // POST: Categories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Categories categories = db.Categories.Find(id);
            db.Categories.Remove(categories);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
              // db.Dispose();
            }
            base.Dispose(disposing);
        }
    }


    
}
