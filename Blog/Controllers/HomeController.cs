﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Blog.Models;

namespace Blog.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {           
            return View();
        }

        // GET: Home
        public ActionResult Index2()
        {
            Categories cat = new Categories()
            {
                idCategorie = 100,
                Libelle = "Programmes"
            };
            return View("Index", cat);
        }

        // GET: Home/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Home/Create
        public ActionResult Create()
        {
            ViewBag.Message = "Bienvenue à Bloggy.";
            return View();
        }  
        
    }
}
