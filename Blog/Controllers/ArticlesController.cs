﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Blog.Models;
using System.Text;

namespace Blog.Controllers
{
    public class ArticlesController : Controller
    {
        private BlogsContext db = new BlogsContext();

        // GET: Articles
        public ActionResult Index()
        {
            var articles = db.Articles.Include(a => a.Categories).Include(a => a.Utilisateurs);
            return View(articles.ToList());
        }

        // GET: Articles/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Articles articles = db.Articles.Find(id);
            if (articles == null)
            {
                return HttpNotFound();
            }
            return View(articles);
        }

        // GET: Articles/Create
        public ActionResult Create()
        {
            ViewBag.idCategorie = new SelectList(db.Categories, "idCategorie", "Libelle");
            ViewBag.idUtilisateur = new SelectList(db.Utilisateurs, "IdUtilisateur", "prenom");
            return View();
        }

        // POST: Articles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idArticle,idUtilisateur,idCategorie,titre,contenu,datePublication")] Articles articles)
        {
            StringBuilder sbContenu = new StringBuilder();
            sbContenu.Append(HttpUtility.HtmlEncode(articles.contenu));
            if (ModelState.IsValid)
            {
                db.Articles.Add(articles);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idCategorie = new SelectList(db.Categories, "idCategorie", "Libelle", articles.idCategorie);
            ViewBag.idUtilisateur = new SelectList(db.Utilisateurs, "IdUtilisateur", "prenom", articles.idUtilisateur);
            return View(articles);
        }

        // GET: Articles/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Articles articles = db.Articles.Find(id);
            if (articles == null)
            {
                return HttpNotFound();
            }
            ViewBag.idCategorie = new SelectList(db.Categories, "idCategorie", "Libelle", articles.idCategorie);
            ViewBag.idUtilisateur = new SelectList(db.Utilisateurs, "IdUtilisateur", "prenom", articles.idUtilisateur);
            return View(articles);
        }

        // POST: Articles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idArticle,idUtilisateur,idCategorie,titre,contenu,datePublication")] Articles articles)
        {
            if (ModelState.IsValid)
            {
                db.Entry(articles).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idCategorie = new SelectList(db.Categories, "idCategorie", "Libelle", articles.idCategorie);
            ViewBag.idUtilisateur = new SelectList(db.Utilisateurs, "IdUtilisateur", "prenom", articles.idUtilisateur);
            return View(articles);
        }

        // GET: Articles/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Articles articles = db.Articles.Find(id);
            if (articles == null)
            {
                return HttpNotFound();
            }
            return View(articles);
        }

        // POST: Articles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Articles articles = db.Articles.Find(id);
            db.Articles.Remove(articles);
            db.SaveChanges();
            return RedirectToAction("Index");
        }


        [HttpGet]
        //[Authorize]
        public ActionResult AjouterCommentaire()
        {
            return PartialView("PartialAjoutCommentaire");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
